<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Mahasiswa</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Kampus</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link" href="mahasiswa.php">Mahasiswa</span></a>
                <a class="nav-item nav-link active" href="#">About <span class="sr-only">(current)</span></a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
    <div class="container">
        <div class="card mt-3">
            <div class="card-header">
                About Page
            </div>
            <div class="card-body">
                <h5 class="card-title">Sistem informasi Kampus</h5>
                <p class="card-text">Sistem ini bertujuan untuk mendukung penyelenggaraan pendidikan, sehingga perguruan tinggi dapat menyediakan layanan informasi yang lebih baik dan efektif kepada komunitasnya, baik didalam maupun diluar perguruan tinggi tersebut melalui internet. Berbagai kebutuhan dalam bidang pendidikan maupun peraturan yang melingkupinya sedemikian tinggi, sehingga pengelolaan akademik dalam suatu lembaga pendidikan menjadi pekerjaan yang sangat menguras waktu, tenaga dan pikiran.</p>
                Dibuat oleh :
                <ul>
                    <li>Kukuh Lutfi Kurniawan - 1931733072</li>
                    <li>Teddy Harfa As'ad Sunaryo - 1931733084</li>
                </ul>
                
            </div>
        </div>
    </div>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
